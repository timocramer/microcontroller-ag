#ifndef UART_H
#define UART_H

#include <avr/io.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

void uart_init(void);
uint8_t uart_get_char(void);
void uart_send_char(uint8_t c);
void uart_get_bytes(uint8_t * const mem, uint8_t length);
void uart_send_bytes(const char * const mem, uint8_t length);

#define uart_send_str(str) \
	uart_send_bytes(str, strlen(str))

#ifdef __cplusplus
}
#endif

#endif
