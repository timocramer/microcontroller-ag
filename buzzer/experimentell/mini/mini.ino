#include <avr/io.h>
#include <util/delay.h>
#include "uart.h"

void setup() {
  pinMode(2, INPUT);  
  pinMode(3, INPUT);  
  pinMode(4, INPUT);  
  pinMode(5, INPUT);  
  pinMode(6, INPUT);  
  uart_init();
}

int check(int in, int out, char* text)
{
  pinMode(out, OUTPUT);
  pinMode(in, INPUT_PULLUP);
  digitalWrite(out, LOW);
  delay (15);
  int gedrueckt = !digitalRead(in);
  if(gedrueckt)
  {
    uart_send_str(text);
  }
  digitalWrite(out,LOW);
  pinMode(out, INPUT);
  return gedrueckt;
}  

int eineTasteGedrueckt = 0;

void loop() {
  
  eineTasteGedrueckt = 0; 
  digitalWrite(2, LOW); //PD0
  digitalWrite(3, LOW); //PD1
  digitalWrite(4, LOW); //PD2
  digitalWrite(5, LOW); //PD3
  digitalWrite(6, LOW); //PD7
  
  eineTasteGedrueckt =   check(4,6,"gelb")   || eineTasteGedrueckt ; //check(out,in,char); ( gemäß liste) 
  eineTasteGedrueckt =   check(6,4,"orange") || eineTasteGedrueckt ; 
  eineTasteGedrueckt =   check(3,6,"grün")   || eineTasteGedrueckt ;
  eineTasteGedrueckt =   check(6,3,"blau")   || eineTasteGedrueckt ;
  eineTasteGedrueckt =   check(2,5,"Buzzer") || eineTasteGedrueckt ;
  
/* LED test
  pinMode(5, OUTPUT);
  pinMode(2, OUTPUT);
  digitalWrite(2, LOW);
  digitalWrite(5, HIGH); 
  delay (1);
  digitalWrite(5, LOW); 
  delay(25); 
*/
  if(eineTasteGedrueckt) uart_send_str("\n");
  delay(100); 
}
