#!/bin/bash

while read line; do
    
	if [ "$line" = "Buzzer" ]; then
		mplayer ./airhorn.mp3 &
	fi
	if [ "$line" = "blau" ]; then
		mplayer ./promovierternaturwisschenschaftler.mp3 &
	fi
	if [ "$line" = "orange" ]; then
		mplayer ./magieistphysik.mp3 &
	fi
	if [ "$line" = "grün" ]; then
		mplayer ./diesonneistkalt.mp3 &
	fi
	if [ "$line" = "gelb" ]; then
		mplayer ./mussmanwissen.mp3 &
	fi

done
