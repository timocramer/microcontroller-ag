#include "uart.h"

#define BAUD 57600

#include <avr/io.h>
#include <util/setbaud.h>

#ifndef UBRRH
#define UBRRH UBRR0H
#endif
#ifndef UBRRL
#define UBRRL UBRR0L
#endif
#ifndef UCSRA
#define UCSRA UCSR0A
#endif
#ifndef UCSRB
#define UCSRB UCSR0B
#endif
#ifndef UCSRC
#define UCSRC UCSR0C
#endif
#ifndef UDR
#define UDR UDR0
#endif
#ifndef UDRE
#define UDRE UDRE0
#endif
#ifndef RXC
#define RXC RXC0
#endif
#ifndef UCSZ0
#define UCSZ0 UCSZ00
#endif
#ifndef UCSZ1
#define UCSZ1 UCSZ01
#endif
#ifndef RXEN
#define RXEN RXEN0
#endif
#ifndef TXEN
#define TXEN TXEN0
#endif
#ifndef U2X
#define U2X U2X0
#endif

void uart_init(void) {
	UBRRH = UBRRH_VALUE;
	UBRRL = UBRRL_VALUE;
	
	if(USE_2X) {
		UCSRA |= _BV(U2X);
	}
	else {
		UCSRA &= ~_BV(U2X);
	}
	
	UCSRB |= _BV(RXEN) | _BV(TXEN);
	// 8 Bit, No Parity, 1 Stop-Bit
	UCSRC |= _BV(UCSZ0) | _BV(UCSZ1);
}

uint8_t uart_get_char(void) {
	loop_until_bit_is_set(UCSRA, RXC);
	return UDR;
}

void uart_send_char(uint8_t c) {
	loop_until_bit_is_set(UCSRA, UDRE);
	UDR = c;
}

void uart_get_bytes(uint8_t * const mem, uint8_t length) {
	uint8_t i;
	
	for(i = 0; i < length; ++i)
		mem[i] = uart_get_char();
}

void uart_send_bytes(const char * const mem, uint8_t length) {
	uint8_t i;
	
	for(i = 0; i < length; ++i)
		uart_send_char(mem[i]);
}
