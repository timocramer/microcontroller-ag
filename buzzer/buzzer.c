#include <avr/io.h>
#include <util/delay.h>

#include "uart.h"

int main(void) {
	uint8_t pressed = 0;
	
	DDRB |= _BV(PB5);
	
	DDRD |= (1 << PD5);
	DDRD &= ~(1 << PD2);
	
	PORTD |= (1 << PD2);
	PORTD &= ~(1 << PD5);
	
	uart_init();
	
	while(1) {
		if(PIND & (1 << PD2)) {
			if(pressed) {
				pressed = 0;
			}
		}
		else {
			if(!pressed) {
				uart_send_str("airhorn\n");
				pressed = 1;
			}
		}
	}
}
