EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "21 oct 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATMEGA8A-P IC1
U 1 1 5627B242
P 2900 3000
F 0 "IC1" H 2150 4300 40  0000 L BNN
F 1 "ATMEGA8A-P" H 3400 1550 40  0000 L BNN
F 2 "DIL28" H 2900 3000 30  0000 C CIN
F 3 "~" H 2900 3000 60  0000 C CNN
	1    2900 3000
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5627B259
P 4250 2700
F 0 "R1" V 4330 2700 40  0000 C CNN
F 1 "470" V 4257 2701 40  0000 C CNN
F 2 "~" V 4180 2700 30  0000 C CNN
F 3 "~" H 4250 2700 30  0000 C CNN
	1    4250 2700
	-1   0    0    1   
$EndComp
$Comp
L LED D1
U 1 1 5627B276
P 4250 3450
F 0 "D1" H 4250 3550 50  0000 C CNN
F 1 "LED" H 4250 3350 50  0000 C CNN
F 2 "~" H 4250 3450 60  0000 C CNN
F 3 "~" H 4250 3450 60  0000 C CNN
	1    4250 3450
	0    1    1    0   
$EndComp
$Comp
L C C1
U 1 1 5627B2AA
P 1600 2900
F 0 "C1" H 1600 3000 40  0000 L CNN
F 1 "100nF" H 1606 2815 40  0000 L CNN
F 2 "~" H 1638 2750 30  0000 C CNN
F 3 "~" H 1600 2900 60  0000 C CNN
	1    1600 2900
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 5627B2BA
P 1600 1500
F 0 "#PWR?" H 1600 1600 30  0001 C CNN
F 1 "VCC" H 1600 1600 30  0000 C CNN
F 2 "" H 1600 1500 60  0000 C CNN
F 3 "" H 1600 1500 60  0000 C CNN
	1    1600 1500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5627B2C9
P 1600 4650
F 0 "#PWR?" H 1600 4650 30  0001 C CNN
F 1 "GND" H 1600 4580 30  0001 C CNN
F 2 "" H 1600 4650 60  0000 C CNN
F 3 "" H 1600 4650 60  0000 C CNN
	1    1600 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 1600 1600 1600
Wire Wire Line
	1600 1500 1600 2700
Connection ~ 1600 1600
Wire Wire Line
	1600 3100 1600 4650
Wire Wire Line
	1600 4500 4250 4500
Connection ~ 1600 4500
Wire Wire Line
	2000 2100 1600 2100
Connection ~ 1600 2100
Wire Wire Line
	2000 2300 1850 2300
Wire Wire Line
	1850 2300 1850 3200
Wire Wire Line
	1850 3200 1600 3200
Connection ~ 1600 3200
Wire Wire Line
	3900 2000 4250 2000
Wire Wire Line
	4250 2000 4250 2450
Wire Wire Line
	4250 2950 4250 3250
Wire Wire Line
	4250 4500 4250 3650
Connection ~ 2900 4500
$EndSCHEMATC
