\documentclass[12pt, dvipsnames]{beamer}

\usetheme{Berlin}
\usecolortheme{whale}

\makeatletter
\beamer@theme@subsectionfalse
\makeatother
\setbeamertemplate{navigation symbols}{}%remove navigation symbols

% add frame numbers
\expandafter\def\expandafter\insertshorttitle\expandafter{%
	\insertshorttitle\hfill%
	\insertframenumber\,/\,\inserttotalframenumber}

\hypersetup{pdfstartview={Fit}}

\usepackage{ngerman}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{lmodern}
\usepackage{amsmath,amssymb,wasysym}
\usepackage{xspace}
\usepackage{eurosym}
\usepackage{siunitx}

\usepackage{hyperref}
\definecolor{links}{HTML}{722F37}
\hypersetup{colorlinks,linkcolor=,urlcolor=links}

\usepackage{listings}
\lstset{
	basicstyle=\ttfamily\scriptsize,
	tabsize=4,
	language=C++,
	commentstyle=\color{OliveGreen},
	keywordstyle=\color{blue},
	stringstyle=\color{red}
}
\lstset{
	morekeywords={uint8_t, uint16_t}
}

\usepackage{pgfgantt}
\usepackage{tikz}
\usetikzlibrary{arrows}
\usetikzlibrary{automata}
\usetikzlibrary{shapes}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{positioning}
\usetikzlibrary{matrix}
\usetikzlibrary{fit}
\usetikzlibrary{calc}
\usetikzlibrary{shapes.misc}
\usetikzlibrary{decorations.pathreplacing}

\newcommand{\etc}{etc\@.\xspace}
\newcommand{\zB}{z\@.\,B\@.\xspace}
\newcommand{\ggf}{ggf\@.\xspace}

\newcommand{\cpp}{{C\nolinebreak[4]\hspace{-.05em}\raisebox{.4ex}{\tiny\textbf{++}}}\xspace}
\newcommand{\kb}{KB\xspace}

\newcommand{\freezeframe}{
	\only<1>{
		\addtocounter{framenumber}{-1} % es ist eigentlich immer noch der selbe frame
	}
}

\title{Microcontroller-Grundlagen}
\author{Timo Cramer}

\begin{document}

\begin{frame}
	\titlepage
\end{frame}

\section{Einleitung}
\begin{frame}
	\frametitle{Definition}
	Was sind Microcontroller?
	
	\begin{columns}
	\begin{column}{0.8\textwidth}
	\begin{itemize}
	\item Ein-Chip-Computersysteme
		\begin{itemize}
		\item Prozessor, RAM, Peripherie, \etc in einem
		\end{itemize}
	\item meist wenige \kb Speicher
	\item sehr geringer Stromverbrauch
	\item sehr geringer Preis
	\item Echtzeitfähigkeit
	\item direkter Hardware-Zugriff
	\end{itemize}
	\end{column}
	
	\begin{column}{0.2\textwidth}
	\includegraphics[width=\textwidth]{R6511}\\
	\includegraphics[width=\textwidth]{ATMEL-AT90S2333}\\
	\includegraphics[width=\textwidth]{PIC18F8720}
	
	\cite{r6511, at90, pic}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Motivation}
	Warum halte ich einen Vortrag darüber?
	\begin{itemize}
	\item Sie werden \textit{überall} verwendet!
		\begin{itemize}
		\item Unterhaltungselektronik
		\item KFZ
		\item Chipkarten
		\item \dots
		\end{itemize}
	
	\pause
		{\footnotesize
		\begin{block}{Wikipedia -- Microcontroller}
		A typical home in a developed country is likely to have only four
		general-purpose microprocessors but around three dozen microcontrollers.
		A typical mid-range automobile has as many as 30 or more microcontrollers.
		\end{block}}
	
	\pause
	\item Trotzdem werden sie selten in Lehrveranstaltungen angesprochen
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Architekturen}
	
	Es gibt viele konkurrierende Hersteller und Architekturen
	\begin{itemize}
	\item Harvard"~ vs. Von-Neumann-Architektur
	\item RISC vs. CISC
	\item 4"~, 8"~, 16"~, 32-Bit
	\item Hardware-Multiplikation \etc
	\end{itemize}
	
	\pause
	Beispiele:
	\begin{itemize}
	\item Texas Instruments mit MSP430
	\item Microchip mit PIC
	\item Atmel mit AVR
	\item viele, viele mehr
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Beispiel: ATmega8A\cite{atmega8a-datasheet}}
	\begin{columns}
	\begin{column}{0.45\textwidth}
		\textbf{Speicher \& CPU}
		\begin{itemize}
		\item 8\,\kb Flash-Speicher
		\item 1\,\kb RAM
		\item 512\,Byte EEPROM
		\item 8-Bit AVR-Architektur (RISC)
		\end{itemize}
	\end{column}
	
	\begin{column}{0.55\textwidth}
		\begin{center}
		\includegraphics[width=\textwidth]{atmegas}
		
		\scriptsize (Abbildung ähnlich)
		\end{center}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Beispiel: ATmega8A\cite{atmega8a-datasheet}}
	\freezeframe
	
	\begin{columns}
	\begin{column}{0.4\textwidth}
		\textbf{Peripherie}
		\begin{itemize}
		\item 28--32 Pins
		\item GPIO
		\item I$^2$C\,\only<2>{\textcolor{yellow}{$\blacksquare$}}
		\item SPI\,\only<2>{\textcolor{green}{$\blacksquare$}}
		\item UART\,\only<2>{\textcolor{blue}{$\blacksquare$}}
		\item A/D"~Wandler\,\only<2>{\textcolor{red}{$\blacksquare$}}
		\item PWM\,\only<2>{\textcolor{cyan}{$\blacksquare$}}
		\end{itemize}
	\end{column}
	
	\begin{column}{0.6\textwidth}
		\begin{overlayarea}{\textwidth}{0.7\textheight}
		\includegraphics<1>[width=\textwidth]{atmega8a-dip}
		\includegraphics<2>[width=\textwidth]{atmega8a-dip-color}
		\includegraphics<3>[width=\textwidth]{atmega8a-tqfp}
		\end{overlayarea}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Beispiel: ATmega8A\cite{atmega8a-datasheet}}
	\freezeframe
	
	\begin{center}
		\includegraphics[width=0.7\textwidth]{stromverbrauchskurve}
	\end{center}
	\vspace{-5ex}\textbf{Energie}\vspace{-1ex}
	\begin{columns}
	\begin{column}{0.5\textwidth}
	\begin{itemize}
	\item 2.7--5\,V Betriebs\-spannung
	\item bis zu 16\,MHz
	\item 5 Schlafmodi
	\end{itemize}
	\end{column}
	
	\begin{column}{0.5\textwidth}
	\textit{Beispiel:}\\3.6\,mA Stromverbrauch im active~mode bei 4\,MHz und 3\,V
	\end{column}
	\end{columns}
\end{frame}


\section{Programmierung}
\begin{frame}
	\frametitle{Programmierung}
	\begin{itemize}
	\item Hardware-nahe Sprachen
		\begin{itemize}
		\item Assembler
		\item C
		\item \cpp
		\end{itemize}
	\item Einschränkungen
		\begin{itemize}
		\item keine Speicherverwaltung mit \texttt{malloc} oder \texttt{new}
		\item keine Dateioperationen, Streams \etc
		\item[$\Rightarrow$] keine komplette libc
		\end{itemize}
	\item Memory-Mapped I/O
	\item häufig kommerzielle Compiler
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Bit-Operationen}
	
	\begin{itemize}
	\item viele Informationen sind in einzelnen Bits gespeichert
	\item Speicherzugriffe erfolgen in Byte-Granularität
	\item Setzen/Löschen von Bits mit Hilfe von \textbar{}, \& und \textasciitilde
	\end{itemize}
	~\\
	\begin{tabular}{c|r|l p{3em} c|r|l}
	         & 0x15 & 00010101 &   &     & 0x15 & 00010101 \\
	\textbar & 0x0c & 00001100 &   &  \& & 0x0c & 00001100 \\
	\cline{1-3}                    \cline{5-7}
	$=$      & 0x1d & 00011101 &   & $=$ & 0x04 & 00000100 \\
	\end{tabular}
	~\\~\\
	\begin{itemize}
	\item Häufig:
		\begin{itemize}
		\item Setzen von Bit n: \lstinline$var |= (1 << n)$
		\item Löschen von Bit n: \lstinline$var &= ~(1 << n)$
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Memory-Mapped I/O}
	
	\begin{itemize}
	\item Einblendung von Registern im Adressraum des Hauptspeichers
	\end{itemize}
	\begin{center}
		\includegraphics[height=0.7\textheight]{data-memory-map}
	\end{center}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Memory-Mapped I/O \& Bit-Operationen}
	
	\begin{onlyenv}<1>
		\begin{itemize}
		\item zur einfacheren Programmierung: \texttt{\#define}s in \texttt{avr/io.h}
		
		\item Beispiel: Setzen eines Bits in PORTB\\
		~\\
		\includegraphics[width=\textwidth]{portb}\\
		~\\
		\begin{lstlisting}
PORTB |= (1 << PB3);
// oder
PORTB |= _BV(PB3);
		\end{lstlisting}
		\end{itemize}
	\end{onlyenv}
	
	\begin{onlyenv}<2>
		\begin{itemize}
		\item Beispiel: Lesen eines Bits in UCSRA\\
		~\\
		\includegraphics[width=\textwidth]{ucsra}\\
		~\\
		\begin{lstlisting}
if(UCSRA & (1 << TXC)) {
	// TXC-Bit ist gesetzt
} else {
	// TXC-Bit ist nicht gesetzt
}
		\end{lstlisting}
		\end{itemize}
	\end{onlyenv}
\end{frame}

\begin{frame}
	\frametitle{"`Hello World"'}
	
	\begin{columns}
	\begin{column}{0.5\textwidth}
	\begin{itemize}
	\item Textausgabe ist schon eher schwierig
	\item einfaches Lebenszeichen: blinkende LED
	\item einfache Schaltung
	\end{itemize}
	\end{column}
	
	\begin{column}{0.5\textwidth}
	\includegraphics[width=\textwidth]{helloworldschaltplan}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame}[fragile]
	\frametitle{"`Hello World"' -- Programm}
	\begin{columns}
	\begin{column}{0.5\textwidth}
	\begin{lstlisting}
#include <avr/io.h>
#include <util/delay.h>

int main(void) {
	// Pin auf Output stellen
	DDRB |= (1 << PB1);
	
	while(1) {
		// Pin B1 auf High
		PORTB |= (1 << PB1);
		_delay_ms(1000);
		// Pin B1 auf Low
		PORTB &= ~(1 << PB1);
		_delay_ms(1000);
	}
	return 0;
}
	\end{lstlisting}
	\end{column}
	
	\begin{column}{0.5\textwidth}
	\includegraphics[width=\textwidth]{atmega8a-dip}
	\begin{description}
	\item[DDRx] Input/Output
	\item[PORTx] High/Low
	\item[PINx] Status
	\end{description}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Kompilieren}
	\begin{lstlisting}[language=bash]
avr-gcc -mmcu=atmega8a -DF_CPU=1000000 -Os \
                     -o helloworld.elf helloworld.c
avr-strip helloworld.elf
	\end{lstlisting}
	
	\begin{itemize}
	\item Chip-Angabe
	\item Taktfrequenz (für \texttt{\_delay\_ms})
	\item möglichst kleines Programm (Optimierung)
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Flashen}
	Wie bekommen wir das Programm jetzt auf den Chip?
	\begin{overlayarea}{\textwidth}{0.7\textheight}
	
	\begin{onlyenv}<1>
	\begin{itemize}
	\item dedizierte Hardware zum Beschreiben des Flash-Speichers
	\item In-System-Programmer (ISP)
	\item viele verschiedene Schnittstellen, häufig
		\begin{itemize}
		\item USB
		\item Serielle Schnittstelle
		\item Parallelport
		\end{itemize}
	\item bei uns: USBASP \raisebox{-5ex}{\includegraphics[height=10ex]{usbasp}}\cite{usbasp-bild}
	\end{itemize}
	\end{onlyenv}
	
	\begin{onlyenv}<2>
	\begin{lstlisting}[language=bash]
avr-objcopy -O ihex -R .eeprom helloworld.elf helloworld.hex
avrdude -p m8 -c usbasp -U flash:w:helloworld.hex:i \
                     -U lfuse:w:0xe1:m -U hfuse:w:0xd9:m
	\end{lstlisting}
	
	\begin{itemize}
	\item Intel Hex-Format
		\begin{itemize}
		\item Auswahl der Sections
		\end{itemize}
	\item Fuse-Bits
		\begin{itemize}
		\item Taktquelle
		\item \ggf Taktfrequenz
		\item Überschreibungsmöglichkeiten einschränken
		\end{itemize}
	\item Tool-Unterstützung, \zB \href{http://www.engbedded.com/fusecalc/}{\texttt{fusecalc}}
	\end{itemize}
	\end{onlyenv}
	
	\end{overlayarea}
\end{frame}

\begin{frame}
	\frametitle{Interrupts}
	
	\begin{itemize}
	\item Hardware-Unterbrechung
	\item danach: Rückkehr ins eigentliche Programm
	\end{itemize}
	
	\begin{ganttchart}[hgrid]{1}{12}
	\ganttbar[name=P1,bar/.append style={fill=OliveGreen}]{Programm}{1}{4} \ganttbar[name=P2,bar/.append style={fill=OliveGreen}]{}{8}{12}\ganttnewline
	\ganttbar[name=ISR,bar/.append style={fill=red}]{ISR}{5}{7}

	\ganttlink{P1}{ISR}
	\ganttlink{ISR}{P2}
	\end{ganttchart}
	
	\begin{itemize}
	\item Behandlungsroutinen selbst definierbar
		\begin{itemize}
		\item ISR (Interrupt-Service-Routine)
		\item zeitkritische Ausführung
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Interrupts -- Quellen}
	
	\begin{columns}
	\begin{column}{0.42\textwidth}
	\begin{itemize}
	\item fest definierte Quellen
		\begin{itemize}
		\item Kommunikation
		\item Timer
		\item Ereignisse
		\end{itemize}
	\item Erlauben/Blockieren mit \texttt{sei}/\texttt{cli}
	\item Maskierung
	\end{itemize}
	\end{column}
	
	\begin{column}{0.58\textwidth}
	\includegraphics[width=\textwidth]{interrupt-tabelle}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Interrupts -- Programmierung}
	
	\begin{columns}
	\begin{column}{0.45\textwidth}
	\begin{lstlisting}
#include <avr/io.h>
#include <avr/interrupt.h>

volatile uint8_t counter = 0;

ISR(INT0_vect) {
	++counter;
}

int main(void) {
	DDRD &= ~(1 << PD2);
	PORTD |= (1 << PD2);
	GICR |= (1 << INT0);
	sei();
	while(1) {
		if(counter >= 5) {
			/* ... */
		}
	}
}
	\end{lstlisting}
	\end{column}
	
	\begin{column}{0.55\textwidth}
% 	\begin{center}
	\hspace{2em}\includegraphics[trim={0 190px 250px 0}, clip, width=0.5\textwidth]{atmega8a-dip}
% 	\end{center}
	\begin{itemize}
	\item Interrupt wenn PD2 auf Low
		\begin{itemize}
		\item Pull-Up per PORTD
		\end{itemize}
	\item wichtig: \texttt{volatile}
		\begin{itemize}
		\item Variable kann sich "`im Hintergrund"' ändern
		\item Einfluss auf Optimierung
		\end{itemize}
	\end{itemize}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Interrupts -- Synchronisierung}
	
	\begin{columns}
	\begin{column}{0.45\textwidth}
	\begin{lstlisting}
#include <avr/io.h>
#include <avr/interrupt.h>

volatile uint16_t counter = 0;

ISR(INT0_vect) {
	++counter;
}

int main(void) {
	uint16_t tmp;
	/* ... */
	cli();
	tmp = counter;
	sei();
	/* ... */
}
	\end{lstlisting}
	\end{column}
	
	\begin{column}{0.55\textwidth}
	\begin{itemize}
	\item Anweisungen sind nicht zwangsläufig atomar
		\begin{itemize}
		\item "`8-Bit Microcontroller"'
		\end{itemize}
	\item Ausschalten von Interrupts
		\begin{itemize}
		\item mit Risiko verbunden
		\end{itemize}
	\item Interrupts während einer ISR sind abgeschaltet
		\begin{itemize}
		\item \dots aber einschaltbar
		\end{itemize}
	\end{itemize}
	\end{column}
	\end{columns}
\end{frame}

\section{Schnittstellen \& Kommunikation}
\begin{frame}
	\frametitle{Schnittstellen \& Kommunikation}
	
	Sehr wichtiges Thema bei Microcontrollern!
	\begin{itemize}
	\item Memory-Mapped I/O
		\begin{itemize}
		\item Spezialregister für Kommunikation
		\end{itemize}
	\item bei komplizierteren Bussen: Zustandsautomaten\\
		\hspace{3em}\includegraphics[height=14ex]{twi-automat}
	\item wenn nicht "`in Hardware"' vorhanden: GPIO-Bit-Banging
		\begin{itemize}
		\item normalerweise nicht effizient
		\item Beispiel: \href{http://vusb.wikidot.com/}{V-USB}
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Beispiel: UART}
	\includegraphics[width=\textwidth]{uart-beispiel.png}
	~\\~\\
	\begin{itemize}
	\item überall zu finden (\zB \includegraphics[height=2ex]{serial-port}\cite{serial-port})
	\item asynchron, Festlegung auf Baudrate
	\item häufig: 8N1, 9600\,Baud
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Beispiel: UART -- Setup}
	
	\begin{columns}
	\begin{column}{0.45\textwidth}
	\begin{lstlisting}
#include <avr/io.h>
#define BAUD 9600
#include <util/setbaud.h>

void uart_init(void) {
	UBRRH = UBRRH_VALUE;
	UBRRL = UBRRL_VALUE;
	
	if(USE_2X) {
		UCSRA |= _BV(U2X);
	}
	
	// erlaube Empfangen und Senden
	UCSRB |= _BV(RXEN) | _BV(TXEN);
	UCSRC |= _BV(UCSZ0) | _BV(UCSZ1); // 8N1
}
	\end{lstlisting}
	\end{column}
	
	\begin{column}{0.55\textwidth}
	\begin{itemize}
	\item \texttt{setbaud.h} definiert
		\begin{itemize}
		\footnotesize
		\item \texttt{UBRRH\_VALUE}
		\item \texttt{UBRRL\_VALUE}
		\item \texttt{USE\_2X}
		\end{itemize}
	\item Einstellungen in den Registern
		\begin{itemize}
		\footnotesize
		\item \texttt{UBRRH}, \texttt{UBRRL} (Baudrate)
		\item \texttt{UCSRA}, \texttt{UCSRB}, \texttt{UCSRC} (Anzahl Bits \etc)
		\end{itemize}
	\end{itemize}
	~\\~\\~\\~\\
	\end{column}
	\end{columns}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Beispiel: UART -- Senden \& Empfangen}
	
	\begin{columns}
	\begin{column}{0.45\textwidth}
	\begin{lstlisting}
uint8_t uart_recv(void) {
	while(!(UCSRA & _BV(RXC))) {
		/* wait */
	}
	uint8_t received = UDR;
	return received;
}

void uart_send(uint8_t to_send) {
	while(!(UCSRA & _BV(UDRE))) {
		/* wait */
	}
	UDR = to_send;
}
	\end{lstlisting}
	\end{column}
	
	\begin{column}{0.55\textwidth}
	\begin{description}
	\item[\texttt{UDR}]  UART Data Register
	\item[\texttt{RXC}] Receive Complete
	\item[\texttt{UDRE}] UART Data Register Empty
	\end{description}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Schnittstellen \& Kommunikation -- Ausblick}
	
	\begin{columns}
	\begin{column}{0.45\textwidth}
	\begin{lstlisting}
ISR(USART_RXC_vect) {
	uint8_t received = UDR;
	fifo_insert(received);
}

uint8_t uart_recv(void) {
	while(fifo_empty()) {
		/* wait */
	}
	
	return fifo_dequeue();
}
	\end{lstlisting}
	\end{column}
	
	\begin{column}{0.55\textwidth}
	\begin{itemize}
	\item Kommunikation mit Hilfe von Interrupts
		\begin{itemize}
		\item Effizienz
		\item Asynchronität
		\item einfacheres Verfolgen des Zustandsautomaten
		\end{itemize}
	\end{itemize}
	\end{column}
	\end{columns}
\end{frame}

\section{Energiesparen}
\begin{frame}
	\frametitle{Energiesparen -- Motivation}
	
	Unser ATmega8A ist immer noch recht hungrig!
	\begin{itemize}
	\item Beispiel: Knopfzelle (3\,V, 150\,mAh)
	\item Zur Erinnerung: bei 4\,MHz Verbrauch von 3.6\,mA
	\item Laufzeit: $$\frac{\SI{150}{mAh}}{\SI{3.6}{mA}} = \SI{41.7}{h}$$
	\end{itemize}
	\pause
	~\\
	Vielleicht muss unser Controller nicht die ganze Zeit laufen\dots
	\begin{itemize}
	\item periodische Aufgaben
	\item Handlung auf Knopfdruck
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Energiesparen -- Motivation}
	\freezeframe
	
	Annahme: Unser Controller muss nur 1\,ms pro Sekunde laufen
	\begin{itemize}
	\item Power-down Mode ($\sim \SI{1}{\micro\ampere}$) für den Rest der Zeit
	\item Laufzeit:
		\begin{eqnarray*}
			\frac{\SI{150}{mAh}}{\SI{3.6}{\milli\ampere} \cdot \frac{1}{1000} + \SI{1}{\micro\ampere} \cdot \frac{999}{1000}} &=& \SI{32616}{\hour}\\
			&=& 3.7\,\text{Jahre}
		\end{eqnarray*}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Energiesparen -- Schlafmodi}
	
	Die Schlafmodi schalten Features eines Controllers ab, \zB
	\begin{itemize}
	\item CPU
	\item Oszillator
	\end{itemize}
% 	\vspace{-1ex}~\\
	\begin{center}
	\begin{tabular}[b]{|l|c|c|c|}
	\hline
	Modus      & Stromaufnahme           & Aufwachzeit \\
	\hline
	\hline
	Active     & \SI{1.2}{\milli\ampere} & -- \\
	\hline
	Idle       & \SI{0.3}{\milli\ampere} & 6\,Takte \\
	\hline
	ADC Noise  & \SI{0.3}{\milli\ampere} & 6\,Takte \\
	Reduction  &                         & \\
	\hline
	Power-down & \SI{0.3}{\micro\ampere} & lang \\
	\hline
	Power-save & \SI{10}{\micro\ampere}  & lang \\
	\hline
	Standby    & \SI{35}{\micro\ampere}  & 6\,Takte \\
	\hline
	\end{tabular}\cite{mikrocontroller-net}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Energiesparen -- Aufwachen}
	
	Wie wachen wir wieder auf?
	\pause
	\begin{itemize}
	\item Durch Interrupts
	\end{itemize}
	\includegraphics[width=\textwidth]{sleep-modes-aufwachen}\cite{atmega8-datasheet}
\end{frame}

\begin{frame}
	\frametitle{Energiesparen -- Ausblick}
	
	Häufig ist der Controller nicht der energiehungrigste Teil
	\begin{itemize}
	\item hungrige Peripherie, \zB Sensoren, Funk
	\item neue Herausforderungen
		\begin{itemize}
		\item An-/Abschalten
		\item Neuinitialisierung vs. Laufenlassen
		\item Daten sammeln und gesammelt versenden
		\end{itemize}
	\end{itemize}
	
\end{frame}

\begin{frame}
	\frametitle{Sonstiges}
	Natürlich können wir hier nicht alles besprechen\dots\\
	Es fehlt \zB
	
	\begin{itemize}
	\item Watchdog-Timer
	\item Taktquellen
	\item Timer/Counter
	\item EEPROM
	\item Bootloader \& Selbst-Programmierung
	\end{itemize}
\end{frame}

\section{AG}
\begin{frame}
	\frametitle{Die Microcontroller-AG}
	
	\begin{itemize}
	\item Gründung im Sommersemester 2015
	\item Ergänzung zur Elektro-AG
	\item Interessenschwerpunkt: digitale Schaltungen und Low-Level-Programmierung
	\item Finanzielle Unterstützung des FSR
	\end{itemize}
	
	Ihr seid eingeladen, mitzumachen!
	
	\begin{itemize}
	\item Vorläufiger Termin: Do ab 14 Uhr im Fachschaftsflur
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Ausstattung}
	
	Eine Grundausstattung ist vorhanden:
	\begin{itemize}
	\item ATmega48PA, ATtiny2313A, ATmega16A
	\item LEDs, Widerstände, Quarze
	\item 433\,MHz-Funkmodule
	\item ein Arduino Uno, ein Raspberry Pi
	\item USB-Programmer, USB-UART-Adapter
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Ausstattung}
	
	Wir entschieden uns für AVR
	\begin{itemize}
	\item geringer Preis (1--2\,\euro{} pro Chip)
	\item freie GCC-Toolchain
	\item Abstraktion durch avr-libc
	\item einfache Programmierwerkzeuge
	\item Arduino
	\end{itemize}
	
	In Zukunft kommt vielleicht MSP430 hinzu.
\end{frame}

\begin{frame}
	\frametitle{Voraussetzungen}
	
	\begin{itemize}
	\item Programmierung
	\end{itemize}
	
	Alles weitere glauben wir, euch beibringen zu können!
\end{frame}

\begin{frame}
	\frametitle{Projektideen}
	
	Wir wollen gern \textit{eure} Projekte verwirklicht sehen, aber wir dachten \zB an\dots
	\begin{itemize}
	\item LEDs blinken lassen (easy)
	\item Videospiel-Controller auslesen (medium easy)
	\item Experimentieren mit 433\,MHz-Funk (medium)
	\item USB-Geräte, \zB Tastatur, Gamepad (medium hard)
	\item ein Betriebssystem schreiben (hard)
	\item[]
	\item Unterstützung der Kiosk-AG
		\begin{itemize}
		\item Karten leer?
		\item Tür zu lange offen?
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Interesse?}
	
	\begin{tabular*}{\textwidth}{@{\extracolsep{\fill} }ccc}
	Wiki & Terminumfrage & Bitbucket\\
	\includegraphics[height=12ex]{qrcode-wiki} & \includegraphics[height=12ex]{qrcode-umfrage} & \includegraphics[height=12ex]{qrcode-bb} \\
	\end{tabular*}
\end{frame}

\begin{frame}[allowframebreaks]
	\footnotesize
	\frametitle{Quellen}
	\bibliographystyle{plain}
	\bibliography{literatur}
\end{frame}

\end{document}
